# M01-operatius

## @edt ASIX-M01 2021-2022

Escola del treball de Barcelona. ASIX Administració de sistemes informàtics.  M01-ISO Implantació de  Sistemes Operatius.

Podeu trobar la documentació LPI mòdul a [ASIX-M01-LPI](https://github.com/edtasixm01)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)

[ASIX-M01](https://gitlab.com/edtasixm01/m01-operatius)  |  [ASIX-M05](https://gitlab.com/edtasixm05/m05-hardware) |  [ASIX-M06](https://gitlab.com/edtasixm06/m06-aso) |  [ASIX-M11](https://gitlab.com/edtasixm11/m11-sad)

---

###  UF1 Instal·lació, configuració i explotació d'un sistema informàtic

 * 01 - Ordres de propòsit general
 * 02 - Ordres de fitxers
 * 03 - Permisos bàsics i avançats
 * 04 - Filtres: filtres, redireccionaments i pipes
 * 05 - Processos
 * 06 - Vim: edició de text

---

### UF2 Gestió de la informació i recursos de xarxa

 * 07 - PER: tractament de text, patrons d'expressions regulars
 * 08 - Scripting bàsic
 * 09 - Shell: bash al complert
 * 10 - Administació d'usuaris

---

### UF3 Implantació de programari específic

 * 11 - Administració de disc
 * 12 - Logs i monitorització
 * 13 - Backups
 * 14 - Arrencada / Systemd / Grub

---

### UF4 Seguretat, rendiment i recursos

 * 15 - Serveis de xarxa
 * 16 -  Administració remota

---
 
