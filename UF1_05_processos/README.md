# M01-operatius

## @edt ASIX-M01 2021-2022

Escola del treball de Barcelona. ASIX Administració de sistemes informàtics.  M01-ISO Implantació de  Sistemes Operatius.

Podeu trobar la documentació LPI mòdul a [ASIX-M01-LPI](https://github.com/edtasixm01)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)


---

###  05 - Processos

 * [Explicacions generals](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/a01-05.pdf)
 * Apunts
   * [Apunts 01 - Apunts del profesor de Processos, Prioritats i tasques programades](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/a01-05-processos_apunts.pdf)
   * [Apunts 02 - ToolBox Running Processes. Capítol 9](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/a01-05-ToolBox_processos.pdf)
   * [Apunts 03 - ToolBox /proc. Apèndic C](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/ToolBox_proc.pdf)
 * Exercicis
   * [15 Exercicis - exercicis de processos, treballs i tasques programades](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/15-A02-05_processos.txt)
 * Apunts LPI
   * [Apunts-501-Processos.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/Apunts-501-Processos.pdf)
   * [Linux Job and Process Control Cheat Sheet.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/Linux%20Job%20and%20Process%20Control%20Cheat%20Sheet.pdf)
   * [gestion_de_procesos.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/gestion_de_procesos.pdf)
   * [bash](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/bash.pdf)
   * [103.5-Exercices.md](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/103.5-Exercices.md)
   * [103.6-Exercices.md](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_05_processos/103.6-Exercices.md)
