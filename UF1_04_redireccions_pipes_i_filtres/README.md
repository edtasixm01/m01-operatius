# M01-operatius

## @edt ASIX-M01 2021-2022

Escola del treball de Barcelona. ASIX Administració de sistemes informàtics.  M01-ISO Implantació de  Sistemes Operatius.

Podeu trobar la documentació LPI mòdul a [ASIX-M01-LPI](https://github.com/edtasixm01)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)


---

###  04 - Filtres: filtres, redireccionaments i pipes

 * [Explicacions generals](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_04_redireccions_pipes_i_filtres/Apunts/a01-04-pipes-i-shell.pdf)
 * Apunts
   * [Apunts 01 - Apunts amb exemples de tota la teoria](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_04_redireccions_pipes_i_filtres/Apunts/a01-04-pipes_filtres_shell_apunts_exercicis.pdf)
   * [Apunts 02 - Apunts amb exemples de filtres i resum d'ordres](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_04_redireccions_pipes_i_filtres/Apunts/a01-04-filtres-resum.pdf)
   * [Apunts 03 - ToolBox Shell and variables](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_04_redireccions_pipes_i_filtres/Apunts/ToolBox_shell_vars.pdf)
   * [stdin / Stdout / Stderr](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_04_redireccions_pipes_i_filtres/Apunts/Apunts-400-stdin_stdout_stderr.pdf)
 * Exercicis
   * [11 exercicis - Exercicis de pipes, redireccionamets i variables](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_04_redireccions_pipes_i_filtres/Exercicis/11-A02-04_pipes_filtres_shell_exercicis_pipes.txt)
   * [14 exercicis - Exercicis de comandes i expansions](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_04_redireccions_pipes_i_filtres/Exercicis/14-A01-04_pipes_filtres_shell_exercicis_comandes_expansion.txt)
   * [103.4-Exerices.md](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_04_redireccions_pipes_i_filtres/Exercicis/103.4-Exerices.md)


