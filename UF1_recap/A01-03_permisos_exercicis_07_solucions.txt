ASIX MP1_ISO
Curs 2010-211
A01-03 Permisos B�sics

===============================
     Permisos B�sics
===============================


Exercici_7
==========
-------------------------------------------------------------------------------
part1:
-------------------------------------------------------------------------------
00) Crear l'estructura de directoris i fitxers:
  a) Com usuari alumne crear un directori de nom mp1/calaix dins de tmp i
     assignar-li els seg�ents permisos: (rwx)(rwx)(---).
	$ mkdir -p /tmp/mp1/calaix
	$ chmod 770 /tmp/mp1/calaix

  b) Crear dos fitxers dins de calaix  de nom fit1 i fit2. Els fitxers
     han de contenir dades. 
	$ ls > /tmp/mp1/calaix/fit1
	$ ls -l > /tmp/mp1/calaix/fit2

  c) Assignar perm��s de lectura dels fitxers al propietari, al grup i a 
     altres.
	$ chmod 444 /tmp/mp1/calaix/fit1 /tmp/mp1/calaix/fit2

01) Crear un usuari user1 (si no existeix) amb password user1
	# useradd -d /tmp/user1 -m user1
	# passwd user1 #(posar user1 de password)
	S'est� canviant la contrasenya de l'usuari user1.
	Nova contrasenya de : 
	Torneu a escriure la nova contrasenya de : 
	CONTRASENYA INCORRECTA: it is too short
	CONTRASENYA INCORRECTA: �s massa senzilla
	passwd: tots els testimonis d'autenticaci� s'han actualitzat amb �xit.

	# finger user1
	Login: user1          			Name: 
	Directory: /tmp/user1               	Shell: /bin/bash
	Never logged in.
	No mail.
	No Plan.

02) Situar-se al directori actiu mp1 i fer des d'aquest directori totes les ordres.
	$ cd /tmp/mp1

03) com a usuari user1 fer les ordres seg�ents (des del directori actiu mp1):
  a) ls calaix
  b) ls -la calaix
  c) cat calaix/fit1
  d) cd calaix
  e) Es pot fer alguna d'aquestes ordres? NO.


04) Assignar a calaix els permisos (rwx)(rwx)(r--) i provar:
  a) ls calaix         --> mostra noms (fit1 i fit2)  + permiso denegado.
  b) ls -la calaix     --> mostra noms (incloent . .. fit1 i fit2) + permiso denegado.
  c) cat calaix/fit1   --> qu� creus que passa?
  d) cd calaix         --> No es poden fer.
	$ chmod 774 calaix
	c) no es pot veure fit1, fit1 t� r, per� calaix no permet x.


05) Assignar a calaix els permisos (rwx)(rwx)(--x) i provar:
  a) ls calaix         --> qu� passa?
  b) ls -la calaix     --> No es poden fer.
  c) cat calaix/fit1   --> Mostra el contingut del fitxer.
  d) cd calaix         --> Ok.
  e) vi calaix/fit1    --> No es pot fer perqu� no es disposa de w en el fitxer.
  f) mv calaix/fit1 calaix/fit1.txt --> No es pot fer perqu� no es disposa del perm��s w en el directori 
	$ chmod 771 calaix


-------------------------------------------------------------------------------
part2:
-------------------------------------------------------------------------------
06) Assignar a calaix els permisos (rwx)(rwx)(r-x) i provar:
  a) ls calaix         --> 
  b) ls -la calaix     --> 
  c) cat calaix/fit1   --> 
  d) cd calaix         --> Tots els anteriors ok.
  e) vi calaix/fit1    --> No es pot fer perqu� no es disposa de w en el fitxer.
  f) mv calaix/fit1 calaix/fit1.txt --> No es pot fer perqu� no es disposa del perm�s w en el directori.
  g) touch calaix/fit3
  h) mkdir calaix/noudir --> no es poden fer els dos �ltims en no disposar del perm�s w en el directori.
	$ chmod 775 calaix


07) Assignar a calaix els permisos (rwx)(rwx)(-w-) i provar:
  a) ls calaix         --> 
  b) ls -la calaix     --> 
  c) cat calaix/fit1   --> 
  d) cd calaix         --> No es pot fer cap d'aquests perqu� falta r i x.
  e) touch calaix/fit3 --> No es pot fer. Tot i disposar de w ens falta x per poder actuar en el directori.
  f) mv calaix/fit1 calaix/fit4.txt --> No es pot fer perqu� no es disposa del perm�s x en el directori.
	$ chmod 772 calaix


08) Assignar a calaix els permisos (rwx)(rwx)(rw-) i provar:
  a) ls calaix         --> Llista noms + permiso denegado (com amb r)
  b) ls -la calaix     --> Idem r.
  c) cat calaix/fit1   --> No es permet, falta x.
  d) cd calaix         --> No es permet, falta x.
  e) touch calaix/fit3 --> No es pot fer. Tot i disposar de w ens falta x per poder actuar en el directori.
  f) mv calaix/fit1 calaix/fit4.txt --> No es pot fer perqu� no es disposa del perm�s x en el directori. La w sense la x no aporta res a la r.
	$ chmod 776 calaix

 
09) Assignar a calaix els permisos (rwx)(rwx)(-wx) i provar:
  a) ls calaix         --> No perm�s, falta r.
  b) ls -la calaix     --> No perm�s falta r.
  c) cat calaix/fit1   --> Ok perqu� tenim x.
  d) cd calaix         --> Ok perqu� tenim x.
  e) touch calaix/fit3 --> Ok perqu� tenim w i x.
  f) mv calaix/fit1 calaix/fit4.txt --> Ok perqu� tenim x.
  g) ls fit1           
  h) ls -l fit1        --> Ok Perqu� la x permet accedit a la info dels fitxers tot i no tenir r.
     			   En realitat la falta de r simplement impedeix fer el llistat del directori. Per� si sabem el nom del que hi ha dins, amb 
  			   la x podem accedir al element de dins (cat, ls...).
  i) cp calaix/fit1 calaix/fit5 --> Ok tenim w en el directori i x, drets administratius en el directori.
  j) rm fit2            --> Ok perqu� es disposa de w i x en el directori.
	$ chmod 773 calaix


10) Assignar a calaix els permisos (rwx)(rwx)(rwx) i provar:
  a) Es pot fer tot el que es vol si els permisos dels fitxers ho permeten.
	$ chmod 773 calaix


-------------------------------------------------------------------------------
part3:
-------------------------------------------------------------------------------
11) Assignar a calaix els permisos (rwx)(rwx)(-wt)
  a) ls calaix         --> No.
  b) ls -la calaix     --> No.
  c) cat calaix/fit1   --> Ok perqu� tenim x.
  d) cd calaix         --> Ok perqu� tenim x.
  e) ls fit1           --> Ok per la r.
  f) ls -l fit1        --> Ok per la x.
  g) touch calaix/fit3 --> Ok perqu� tenim w i x.  
  h) cat > calaix/fit4 --> Ok perqu� tenim w i x.  
  i) cp calaix/fit1 calaix/fit5     --> Ok tenim w i x. 
  j) rm fit2           --> No per l'sticky bit.
  k) mv calaix/fit1 calaix/fit4.txt --> No per l'sticky bit. Si crea
                           fit4 pero no pot eliminar fit1.
	     La t (conjuntament amb la w) permet la x (entrar, veure descripcions...) pero treu els drets administratius de esborrar i 
	     canviar de nom un element. Es a dir, nomes es pot usar la w si l'element es seu, si es d'un altre propietari no.
  l) vi fit3 + editar + desar --> Ok perque es de user1.
  m) vi fit1 + editar + desar --> No perque es de alumne.
  
12) Assignar a calaix els permisos (rwx)(rwx)(--t)
  a) Permet fer el mateix que amb el permis X pero te activat el t,
     que no es pot aplicar perque no tenim w per esborrar coses.
  b) La t es aplicacble als altres permisos de propietari i de grup.
  

13) Assignar a calaix els permisos (rwx)(rwx)(--T)
  a) T no conte x.
  b) L'sticky bit s'aplica a propietari i grup si tenen la w 
     activada pero no es aplicable a altres perque no tenen x.

14) Assignar a calaix els permisos (rwx)(rws)(rwx)     
   a) Tot el que es crea en el directori pertany al grup al que
      pertany el directori.
      
15) Assignar a calaix els permisos (rwx)(rwS)(rwx)
   a) No x per a grup a pesar que esta actiu el setguid.
   b) No te sentit que el grup tingui setguid pero al meteix temps
      es restringueixi la x al grup.

16) Assignar a calaix els permisos (rws)(rwx)(rwx)
   a) No aplicable
       
     			   



