# M01-operatius

## @edt ASIX-M01 2021-2022

Escola del treball de Barcelona. ASIX Administració de sistemes informàtics.  M01-ISO Implantació de  Sistemes Operatius.

Podeu trobar la documentació LPI mòdul a [ASIX-M01-LPI](https://github.com/edtasixm01)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)


---

###  01 - Ordres de propòsit general

 * [Explicacions generals](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_01_ordres_de_proposit_general/a01-01_explicacions.pdf)
 * [01 Exercicis](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_01_ordres_de_proposit_general/01_exercicis_ordres_generals.pdf)
 
