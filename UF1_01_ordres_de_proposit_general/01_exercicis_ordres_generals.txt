﻿Exercicis d’ordres generals (01)
	Conceptes clau:
* Prompt  (user/root)
* Ordre opcions arguments 
* Man


* Sessions de text / gràfiques.
* Iniciar / tancar sessions.
* Editar amb vim.


* Element d’una entrada de directori: fitxer o directori. Atributs.
Ordres a treballar:
Alt+ctrl+F#,  login, logout, exit, ctrl+d
[user@host ~]$, [root@host ~]#
pwd, ctrl+l, ls, ls -l, ls -a, ls -la, ls -all, ls -la | les
vim


Miscel·lània d’ordres generals:
date, cal, who, w, finger, finger user, 
Exercicis
________________
Primera Part
Consultar el man i fer les ordres següents:
1. pwd
2. ls
3. whoami
4. id
5. id root
6. who
7. w
8. uname 
9. uname -a
10. uptime
11. hostname
12. finger
13. finger guest
14. finger root
15. date
16. cal
17. clear
18. reboot
19. roweroff
Segona Part
1. whereis ls
2. which ls
3. help cd
4. ps
5. ps ax
6. pstree
7. ps ax | less
8. ps ax | wc
9. dmesg
10. dmesg | less
11. updatedb
12. locate fstab
13. cat /etc/fstab
14. file /etc/fstab
TerceraPart
1. Esbrinar usant la ordre cal el dia de la setmana del vostre naixement, cal mostrar el calendari del més.