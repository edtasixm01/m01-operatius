# M01-operatius

## @edt ASIX-M01 2021-2022

Escola del treball de Barcelona. ASIX Administració de sistemes informàtics.  M01-ISO Implantació de  Sistemes Operatius.

Podeu trobar la documentació LPI mòdul a [ASIX-M01-LPI](https://github.com/edtasixm01)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)


---

###  02 - Ordres de fitxers

 * [Explcacions generals](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/a01-02_explicacions.pdf)
 * Apunts
   * [00-Creació_de_estructura](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/00-Creacio_de_estructura.pdf)
   * [01-Conceptes_generals](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/01-conceptes_generals.pdf)
   * [02-Arbre_de_directoris](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/02-Arbre_de_directoris.pdf)
   * [03-Mostrar_crear_fitxers_cat](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/03-Mostrar_crear_fitxers_cat.pdf)
   * [04-Llistar](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/04-Llistar_ls.pdf)
   * [05-Pathname_expansion](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/05-Pathname_expansion.pdf)
   * [06- Copiar_fitxers_cp](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/06-Copiar_fitxers_cp.pdf)
   * [07-Moure_fitxers_mv](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/07-Moure_fitxers_directoris_mv.pdf)
   * [08-Esborrar_fitxers_directoris_rmdir_rm](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/08-Esborrar_fitxers_directoris_rmdir_rm.pdf)
   * [09-Recursiu](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/09-Recursiu.pdf)
   * [10-Vim](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Apunts/10-vim.pdf)
 * Exercicis
   * [02 exercicis: crear directoris](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Exercicis/02_exercicis_fitxers.pdf)
   * [03 exercicis: Entrada de directoris, ññistats, Metacaràcters, fitxers ocults](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Exercicis/03_exercicis_fitxers.pdf)
   * [04 exercicis: Enllaços simbòlics i durs, rename, comparar, comprimir, dividir](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Exercicis/04_exercicis_fitxers.pdf)
   * [05 exercicis: Informació d'usuari, quotes, buscar fitxers, empaquetar](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_02_ordres_de_fitxers/Exercicis/05_exercicis_fitxers.pdf)
