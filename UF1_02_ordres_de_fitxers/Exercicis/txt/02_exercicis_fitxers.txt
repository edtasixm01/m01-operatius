===============================================================================
Exercicis de fitxers (02)
===============================================================================
Conceptes clau:
* Directori actiu.
* Ruta relativa.
* Ruta absoluta.

Ordres a treballar:
cd, ls, mkdir, rmdir, cp, mv, rm, tree
vim, echo, ordre > file, date, cal, uname -a, uptime
man, help, whereis, locate, apropos
plantilles:  carta.* carta*.txt *.pdf carta?.txt
file, head, tail, wc, less

Exercicis
===============================================================================
Primera part
1. Mostrar el directori actiu.
2. Fer actiu el directori arrel.
3. Mostrar el directori actual. Llistar (ls) el directori actual.
4. Fer actiu el directori var fill de l’arrel. Mostrar el directori actiu.
5. Fer actiu el directori tmp fill de /var. Mostrar quin és el directori actiu.
6. Fer actiu el directori /tmp. 
7. Llistar el directori (ls) actiu. Fer un llistat llarg del directori actiu.
8. Fer un llistat llarg paginat del directori actiu.
9. Fer actiu el directori /var/tmp.
10. Fer actiu el seu directori pare. Fer l’ordre pwd.
11. Fer actiu el seu directori pare. Fer l’ordre pwd.
12. Fer l’ordre cd. Fer l’ordre pwd.

Segona part
1. Fer actiu el directori /var/tmp.
2. Crear  el directori prova (dins de /var/tmp).
3. Crear el directori m01 (dins de /var/tmp).
4. Fer actiu el directori fill m01.
5. Crear dins de m01 els seguents fitxers:
   1. Amb l’ordre vim crear carta.txt
   2. Amb l’ordre vim crear treball.pdf
   3. Usar ordres del sistema (per exemple who, w, ps, uptime… ) per crear els fitxers carta1.txt, carta2.txt, carta3.txt i carta4.txt.
   4. Usar l’ordre echo per generar els fitxers informe.pdf, investigacio.odt i expedient.odt. 
1. Fer actiu el directori pare (/var/tmp).
2. Crear de cop els directoris operatius i sistemes dins de m01.
3. Crear de cop el directori /tmp/xarxes/captures.
4. Fer un tree -d de /var/tmp

Tercera part
1. Fer actiu l directori actiu /var/tmp.
2. Mostra el contingut (cat) de carta.txt (un fitxer).
3. Llistar el confingut (ls) de m01 (un directori).
4. Llistar el contingut del directori arrel.
5. Llistar el contingut del directori /tmp/xarxes.
6. Copiar el fitxer carta.txt (el directori actiu continua essent /var/tmp) dins de operatius. 
7. Copiar el fitxer carta.txt dins de /tmp/xarxes.
8. Copiar el fitxer carta.txt al directori actiu.
9. Copiar el fitxer carta.txt al directori actiu amb el nou nom lletra.txt
10. Copiar el fitxer carta.txt al directori operatius amb el nou nom lletra.txt.
11. Copiar el fitxer carta.txt al directori xarxes amb un nou nom, lletra.txt.
12. Esborrar un a un cada un dels fitxers lletra creats en els exercicis anteriors (a /var/tmp, a operatius i a xarxes).
13. Moure el fitxer carta.txt que hi ha a operatius a dins de sistemes.
14. Moure el fitxer carta.txt que hi ha a sistemes a dins de captures.
15. Moure el fitxer carta.txt que hi ha dins de captures a dins de operatius, però ara s’ha d’anomenar letter.txt.

Quarta part
1. Verificar que el directori actiu és /var/tmp.
2. Llistar tots els fitxers de m01 que tinguin extensió txt.
3. Llistar tots els fitxers de m01 que tinguin extensió pdf.
4. Llistar tots els fitxers de m01 que comencin per la lletra c.
5. Llistar tots els fitxers del directori m01 que de nom comencin per carta + un caràcter i una extensió txt.
6. Copiar tots els fitxers d’extensió txt del directori m01 al directori /tmp/xarxes/captures.
7. Copiar tots els fitxers d’extensió txt del directori m01 al directori sistemes.
8. Copiar tots els fitxers d’extensió txt del directori sistemes al directori operatius.
9. Eliminar tots els fitxers d’extensió txt del directori captures.
10. Eliminar tots els fitxers que comencin per c del directori sistemes.
11. Moure tots els fitxers del directori operatius al directori captures.

Cinquena part
* Per realitzar aquesta part cal que preparis tu què fer per provar cada concepte tractat en cada exercici.
1. Moure directoris.
2. Canviar de nom directoris.
3. Eliminar directoris buits.
4. Eliminar directoris que tenen contingut.
5. Es poden copiar un conjunt de fitxers a un altre destí i al mateiox temps canviar-los el nom que tenen en el destí?
6. Es poden moure un conjunt de fitxers i indicar múltiples destins?

Tasca final
1. Anotar en un document drive un resum dels conceptes tractats i una llista del tipus d’exercicis fets (no dels exercicis sinó del significat).
