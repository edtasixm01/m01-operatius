# M01-operatius

## @edt ASIX-M01 2021-2022

Escola del treball de Barcelona. ASIX Administració de sistemes informàtics.  M01-ISO Implantació de  Sistemes Operatius.

Podeu trobar la documentació LPI mòdul a [ASIX-M01-LPI](https://github.com/edtasixm01)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)


---

###  03 - Permisos bàsics i avançats

 * [Explicacions generals](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Apunts/a01-03_explicacions.pdf)
 * Apunts
   * [200 Permisos bàsics](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Apunts/200-permisos-basics.pdf)
   * [201 Permisos de directoris](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Apunts/201-permisos-directoris.pdf)
   * [202 Umask / chown / chmod](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Apunts/202-permisos-umask-chown-chgrp.pdf)
   * [203 Permisos simbòlics / executables](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Apunts/203-permisos-simb%C3%B2lics-executables.pdf)
   * [204 Permisos avançats](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Apunts/204-permisos-avan%C3%A7ats.pdf)
   * [205 Permisos avançats de directoris](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Apunts/205-permisos-avan%C3%A7ats-directoris.pdf)
   * [206 Permisos avançats de fitxers](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Apunts/206-permisos-avan%C3%A7ats-fitxers.pdf)
 * Exercicis
   * [01-exercicis_permisos](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Exercicis/01-exercicis_permisos.pdf)
   * [02-exercicis_permisos](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF1_03_permisos/Exercicis/02-exercicis_permisos.pdf)
