******************************************************************************
  M01-ISO Sistemes Operatius
  UF2: Gestió de la informació i recursos de xarxa
******************************************************************************
  A03-09-Administració d'usuaris
  Exercici 26t: RESUM de teoria dels exercicis 26
  
******************************************************************************

------------------------------------------------------------------------
1) Crear usuari.
------------------------------------------------------------------------
a) Crear un usuari. Veure les entrades en els fitxers pertinents.
   El camp passwd conté !!.
b) Activar un compte d'usuari definint un passwd.


------------------------------------------------------------------------
2) Creació per defecte.
------------------------------------------------------------------------
a) Per defecte es crea un grup del mateix nom de l'usuari.
b) Es crea un directori home amb el mateix nom de l'usuari dins del 
   directori /tmp/home. Això es fa per defecte.
c) Per defecte es copia un conjunt de fitxers (l'esquelet) al directori 
   home de l'usuari.
d) S'assigna a l'usuari un shell per defecte.


------------------------------------------------------------------------
3) Modificar la informació de l'usuari.
------------------------------------------------------------------------
a) Editant-la del fitxer pertinent (perillós!).
b) Usermod -....opcions.
c) Usant les ordres apropiades per a cada concepte: chfn, chsh, passwd, 
   .plan...


------------------------------------------------------------------------
4) Significat del camp passwd.
------------------------------------------------------------------------
a) Compte no activat: !!.
b) Compte bloquejat temporalment: !passwd-crypt.
c) Compte desactivat: *.
d) Compte a Shadow: X.
e) Compte normal: passwd-crypt.
f) Root pot modificar els passwd dels usuaris sense saber l'anterior i 
   sense haver de seguir el passwd les normes de la política de 
   contrasenyes.
g) Un usuari ha de recordar el passwd anterior per poder-lo canviar. El 
   nou passwd ha de complir les normes marcades per als passwd.


------------------------------------------------------------------------
5) Esborrar usuaris.
------------------------------------------------------------------------
a) S'esborra l'entrada de l'usuari dels fitxers pertinents.
b) No s'elimina ni el seu directori home ni tot allò que conté. A no ser 
   que s'especifiqui l'opció -r de userdel.
c) Tot i així queden en el sistema tots els fitxers propietat de l'usuari
   que es troben en altres directoris. Cal que l'administrador els esborri
manualment, tot localitzant-los (fer un find amb l'opció que el owner 
   sigui l'usuari i un |xargs rm).


------------------------------------------------------------------------   
6) Grups.
------------------------------------------------------------------------
a) En crear un usuari per defecte es crea un grup al que l'usuari pertany 
   com a grup principal. Aquest grup té el mateix nom que l'usuari.
b) Tot usuari pertany almenys a un grup principal.
c) Tot usuari pot pertànyer a més grups.
d) Per modificar grups: editar els fitxers pertinents o usar groupmod.
e) No es pot eliminar un grup si conté usuaris dels quals és grup principal.
f) Si es pot esborrar un grup si tots els usuaris que hi pertanyen tenen un
   altre grup com a grup principal.


------------------------------------------------------------------------
7) Modificar les opcions:
------------------------------------------------------------------------
a) Totes les opcions en la creació d'usuaris i grups es poden especificar
   en el moment de fer la creació (opcions de les ordres useradd i 
   groupadd).
b) Si no s'especifica alguna opció s'utilitzen els valors per defecte.
c) Cal saber les opcions de: crear o no el directori home, on es el 
   directori, quin és l'esquelet, crear o no el grup associat, quin és el 
   grup principal, quins els altres grups, quin és el shell i quin és el 
   uid o el gid.


------------------------------------------------------------------------
8) Opcions per defecte.
------------------------------------------------------------------------
a) useradd -D.
b) /etc/default/useradd
c) /etc/login.def


------------------------------------------------------------------------
9) esquelets.
------------------------------------------------------------------------
a) /etc/skel.


------------------------------------------------------------------------
10) Sistema de passwd a l'ombra.
------------------------------------------------------------------------
a) /etc/shadow i /etc/gshadow.


------------------------------------------------------------------------
11) Política de passwd.
------------------------------------------------------------------------
a) Mida mínima.
b) Dies mínim entre canvis.
c) Dies màxim sense canvis.
d) periòde de warning anterior al màxim
e) període de inactivity posterior al màxim
f) data absoluta d'expiració del compte
g) data absoluta de l'últim canvi del password.
Mapa:

--x---min---------warning-------max-----inactivity-----
x: data de l'ultim cànvi
min: han de passar min dies per poder-lo tornar a canviar
max: s'ha de canviar abans de max dies (caduca el passwd)
warning: avisar n dies abans de max de que caduca el passwd
inactivity: dies en que passat max es deixa iniciar sessió
            obligant a canviar el passwd

--x--------------------------------------account---------
account: data absoluta en que el compte d'usuari caduca        
