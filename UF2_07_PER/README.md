# M01-operatius

## @edt ASIX-M01 2021-2022

Escola del treball de Barcelona. ASIX Administració de sistemes informàtics.  M01-ISO Implantació de  Sistemes Operatius.

Podeu trobar la documentació LPI mòdul a [ASIX-M01-LPI](https://github.com/edtasixm01)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)


---

###  07 - PER: tractament de text, patrons d'expressions regulars

 * Apunts
   * [Apunts 01 - Apunts del profesor de PER (patrons d'expressió regulars) i tractament de text](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/A03-06-PER_apunts.pdf)
   * [Apunts 02 - ToolBox Manipulating text, Capítol 5. ToolBox_PER_text.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/ToolBox_PER_text.pdf)
   * [Apunts 03 - ToolBox Using vi or vim editors, Apèndix A. ToolBox_vi.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/ToolBox_vi.pdf)
 * Exercicis
   * [01-Exercicis grep, tr, cut, sort, sed i camps](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/01-exercicis-per.pdf)
 * Apunts ioc
   * [Apunts-300-regular_expressions.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/Apunts-300-regular_expressions.pdf)
   * [Apunts_301-grep.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/301-grep.pdf)
   * [Apunts_302-cut.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/302-cut.pdf)
   * [Apunts_303-sort.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/303-sort.pdf)
   * [Apunts_304-tr_normalitzar.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/304-tr_normalitzar.pdf)
 * Apunts LPI
   * [expresiones_regulares.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/expresiones_regulares.pdf)
   * [Laboratorio Cadenas de texto y filtros.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/laboratorio%20Cadenas%20de%20texto%20y%20filtros.pdf)
   * [Laboratorio manipulación de texto en sistemas basados en UNIX.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/Laboratorio%20manipulaci%C3%B3n%20de%20texto%20en%20sistemas%20basados%20en%20UNIX.pdf)
   * [103.2-Exercices.md](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/103.2-Exercices.md)
   * [103.7-Exercices.md](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/103.7-Exercices.md)
   * [103.7-Exercices_alternate_2.pdf](https://gitlab.com/edtasixm01/m01-operatius/-/blob/main/UF2_07_PER/103.7-Exercices_alternate_2.pdf)
   
